# ses-service mender module
Based on https://hub.mender.io/t/directory/325 but for stopping and starting a specific service when updating.

Example:
```
ARTIFACT_NAME="my-update-1.0"
DEVICE_TYPE="my-device-type"
OUTPUT_PATH="my-update-1.0.mender"
DEST_DIR="/"
OVERLAY_TREE="rootfs_overlay"

SERVICE=assetctrl

ses-docker-artifact -n ${ARTIFACT_NAME} -t ${DEVICE_TYPE} -d ${DEST_DIR} -o ${OUTPUT_PATH} ${OVERLAY_TREE} ${SERVICE}
```
